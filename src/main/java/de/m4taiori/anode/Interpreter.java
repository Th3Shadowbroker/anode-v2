/*
 * The MIT License
 *
 * Copyright 2017 Jens F..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.m4taiori.anode;

import de.m4taiori.anode.Addons.AddonDirectory;
import de.m4taiori.anode.Util.Console;

/**
 * The central interpretation class.
 * 
 * @author Jens F.
 */
public class Interpreter 
{
    /**
     * The software-version.
     */
    private static final String Version = "1.0.0";
    
    /**
     * Main-method.
     * 
     * @param args Start-arguments.
     */
    public static void main( String[] args )
    {
        if ( args.length > 0 )
        {
            String KeyArgument = args[0].replaceFirst("-", "").toLowerCase();
            
            //Interpret the key argument.
            switch ( KeyArgument )
            {
                
                //Post a list of all file in the addon-directory
                case "ls":
                    if ( AddonDirectory.getInstance().getFileNames().isEmpty() )
                    {
                        Console.println( "No files in addon directory." );
                    }
                    else
                    {
                        AddonDirectory.getInstance().getFileNames().forEach((AddonName) -> 
                        {
                            Console.println( AddonName );
                        }
                        );
                    }
                    break;
                
                //Post version-info
                case "version":
                    Console.println( "ANode v" + Version );
                    Console.println( "Writte by Jens F." );
                    break;
                    
                //Post help
                case "help":
                    Console.println( "====[ANode-Help]====\n" );
                    Console.println( "-help: Access this screen.\n" );
                    Console.println( "-version: Get installed ANode version.\n" );
                    break;
                    
            }
            
        }
        else
        {
            Console.println( "No valid arguments. Use -help for help!" );
        }
    }
}
