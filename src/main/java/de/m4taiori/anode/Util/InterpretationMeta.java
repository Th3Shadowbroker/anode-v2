/*
 * The MIT License
 *
 * Copyright 2017 Jens F..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.m4taiori.anode.Util;

import de.m4taiori.anode.Abstract.Interpretation;

/**
 * An interpretation-classes meta-data.
 * 
 * @author Jens F.
 */
public class InterpretationMeta 
{
    
    //<editor-fold defaultstate="collapsed" desc="Variables">
    
    /**
     * The interpretation class.
     */
    private final Interpretation InterpretationClass;
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Construction">
    
    /**
     * Construction of this class.
     * 
     * @param interpretation The meta origin.
     */
    public InterpretationMeta( Interpretation interpretation )
    {
        
        this.InterpretationClass = interpretation;
        
    }
    
    //</editor-fold>
 
    //<editor-fold defaultstate="collapsed" desc="Methods">
    
    /**
     * Meta is set ?
     * 
     * @return False if not.
     */
    public boolean metaIsSet()
    {
        return this.InterpretationClass.getClass().isAnnotationPresent( Interpretation.Info.class );
    }
    
    /**
     * Get the author.
     * 
     * @return The author.
     */
    public String getAuthor()
    {
        return this.InterpretationClass.getClass().getAnnotation( Interpretation.Info.class ).author();
    }
    
    /**
     * Get the description.
     * 
     * @return The description.
     */
    public String getDescription()
    {
        return this.InterpretationClass.getClass().getAnnotation( Interpretation.Info.class ).description();
    }
    
    /**
     * Get the addon-key.
     * 
     * @return The key.
     */
    public String getKey()
    {
        return this.InterpretationClass.getClass().getAnnotation( Interpretation.Info.class ).key();
    }

    //</editor-fold>
    
}
