/*
 * The MIT License
 *
 * Copyright 2017 Jens F..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.m4taiori.anode.Addons;

import de.m4taiori.anode.Abstract.Interpretation;
import de.m4taiori.anode.Util.Console;
import de.m4taiori.anode.Util.InterpretationMeta;
import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 *
 * @author Jens F.
 */
public class AddonManager 
{
    
    //<editor-fold defaultstate="collapsed" desc="Variables">
    
    /**
     * The current instance of this class.
     */
    private static AddonManager Instance;
    
    /**
     * All interpretations.
     */
    private List<Interpretation> Interpretations = new ArrayList<>();
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Construction">
    
    /**
     * Construction
     */
    public AddonManager()
    {
        
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Methods">
    
    /**
     * Initialization including addon-loading sequence.
     */
    private void init()
    {
        for ( File AddonJar : AddonDirectory.getInstance().getAddonDir().listFiles() )
        {
            //Is jar
            if ( AddonJar.getName().endsWith( ".jar" ) )
            {
                try
                {
                    
                    //Set variables
                    JarFile AddonFile = new JarFile(AddonJar);
                    Enumeration<JarEntry> AddonFileEntries = AddonFile.entries();
                    
                    //Check all entries using while
                    while ( AddonFileEntries.hasMoreElements() )
                    {
                        
                        //The current entry
                        JarEntry CurrentEntry = AddonFileEntries.nextElement();
                        
                        //Is class
                        if ( CurrentEntry.getName().endsWith( ".class" ) )
                        {
                            if ( CurrentEntry.getClass().isAssignableFrom( Interpretation.class ) )
                            {
                                
                                InterpretationMeta CurrentMeta = new InterpretationMeta( Interpretation.class.cast( CurrentEntry ) );
                                
                                Console.println( "Found addon-class: " + CurrentMeta.getKey() );
                                
                            }
                        }
                        
                    }
                    
                }
                catch ( Exception ex )
                {
                    Console.println( "Error while loaing addon \"" + AddonJar.getName() + "\"..." );
                    ex.printStackTrace();
                }
            }
        }
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Static">
    
    /**
     * Get the current instance of this class.
     * 
     * @return The current instance of this class.
     */
    public static AddonManager getInstance()
    {
        if ( Instance == null )
        {
            Instance = new AddonManager();
        }
        return Instance;
    }
    
    //</editor-fold>
    
}
