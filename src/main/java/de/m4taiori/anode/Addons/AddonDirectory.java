/*
 * The MIT License
 *
 * Copyright 2017 Jens F..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.m4taiori.anode.Addons;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents the addon-directory.
 * 
 * @author Jens F.
 */
public class AddonDirectory 
{

    //<editor-fold defaultstate="collapsed" desc="Variables">
    
    /**
     * The current instance of this class.
     */
    private static AddonDirectory Instance;
    
    /**
     * ANode startup dir.
     */
    private final File StartupPath = new File( System.getProperty( "user.dir" ) );
    
    /**
     * ANodes addon dir.
     */
    private final File AddonDir = new File( StartupPath + "/addons" );
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Methods">
    
    /**
     * Get a list of all files contained in the addon-directory.
     * 
     * @return A list of all known files in this directory.
     */
    public List<String> getFileNames()
    {
        
        List<String> Tmp = new ArrayList<>();
        
        if ( AddonDir.exists() )
        {
            for ( File AddonFile : AddonDir.listFiles() )
            {
                Tmp.add( AddonFile.getName() );
            }
        }
        else
        {
            AddonDir.mkdir();
        }
        
        return Tmp;
        
    }
    
    /**
     * Get the addon-directory.
     * 
     * @return The addon directory.
     */
    public File getAddonDir()
    {
        return AddonDir;
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Static">
    
    /**
     * Get the current instance of this class.
     * 
     * @return The current instance.
     */
    public static AddonDirectory getInstance()
    {
        if ( Instance == null )
        {
            Instance = new AddonDirectory();
        }
        return Instance;
    }
    
    //</editor-fold>
    
}
