/*
 * The MIT License
 *
 * Copyright 2017 Jens F..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.m4taiori.anode.Abstract;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Extended by classes wich act as a sperated interprtation routine.
 * 
 * @author Jens F.
 */
public abstract class Interpretation 
{
    
    /**
     * An interpretations info-annotation.
     */
    @Retention( RetentionPolicy.RUNTIME )
    public static @interface Info
    {
        /**
         * The interpretation-author.
         * @return The authors name.
         */
        public String author();
        
        /**
         * The key this class is using
         * @return The key.
         */
        public String key();
        
        /**
         * The interpretation-description.
         * @return The description.
         */
        public String description();
        
        /**
         * The interpretation-usage.
         * @return The usage.
         */
        public String usage();
    }            
    
    /**
     * Loaded when detected by ANode.
     */
    public abstract void onLoad();
    
    /**
     * Loaded during interpretation.
     */
    public abstract void onInterpretation();
    
}
